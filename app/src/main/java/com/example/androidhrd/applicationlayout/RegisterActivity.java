package com.example.androidhrd.applicationlayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {

    Button btnBack;
    TextView txtname,txtphone,txtclass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        btnBack=findViewById(R.id.btn_Back);
        txtclass=findViewById(R.id.tv_showClass);
        txtname=findViewById(R.id.tv_showName);
        txtphone=findViewById(R.id.tv_showPhone);

//        String name=getIntent().getStringExtra("student_name").toString();
//        String phone=getIntent().getStringExtra("student_phone").toString();
//        String className=getIntent().getStringExtra("student_classname").toString();

        Student student= (Student) getIntent().getSerializableExtra("mystudent");

        txtphone.setText(student.getPhone()+"");
        txtname.setText(student.getName()+"");
        txtclass.setText(student.getClassName()+"");

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                setResult(1,intent);
                finish();
            }
        });

    }
}
